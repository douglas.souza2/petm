package br.edu.cest.pet.dev;

public class Book extends Item {
	public String author;
	public String edition;
	public String volume;
	
	public Book(String author) {
		super(author);
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}

}
