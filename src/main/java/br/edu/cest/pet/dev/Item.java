package br.edu.cest.pet.dev;

public class Item {
	public String title;
	public String publisher;
	public String yearpublished;
	public String isbn;
	public String price;
	
	public Item(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYearpublished() {
		return yearpublished;
	}
	public void setYearpublished(String yearpublished) {
		this.yearpublished = yearpublished;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

}
