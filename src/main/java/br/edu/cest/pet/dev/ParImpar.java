package br.edu.cest.pet.dev;

public class ParImpar {
	
	public static void main(String[] args) {
		int num = 2;
				
		if(num % 2 == 0) {
			System.out.println("O número é par!");
		}else {
			System.out.println("O número é impar!");
		}
		
	}

}
