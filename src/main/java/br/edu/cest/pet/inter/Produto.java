package br.edu.cest.pet.inter;

public class Produto implements IPagamento {
	private String nome;
	private int codigo;
	private float preco;

	public Produto(String n, Integer cod, float p) {
		nome = n;
		codigo = cod;
		preco = p;
	}
	
	@Override
	public String getNome() {
		return nome;
	}
	
	@Override
	public Integer getCodigo() {
		return codigo;
	}
	
	@Override
	public float getPreco() {
		return preco;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("---------------------\n");
		sb.append("Nome: " + nome + "\n");
		sb.append("Código: " + codigo + "\n");
		sb.append("Preço: " + preco + "\n");
		return sb.toString();
	}

}
