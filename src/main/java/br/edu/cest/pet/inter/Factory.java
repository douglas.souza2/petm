package br.edu.cest.pet.inter;

import java.util.ArrayList;
import java.util.Scanner;

public class Factory {
	ArrayList<IPagamento> lista = new ArrayList<IPagamento>();
	private String n;
	private int cod;
	private float p;
	
	public boolean registro(String n, Integer cod, float p) {
		boolean re = true;
		IPagamento pag = new Produto(n, cod, p);
		re = lista.add(pag);
		return re;
	}
	
	public static void main(String[] args) {
		Factory f = new Factory();
		 f.pagamento();
	}

	private void pagamento() {
		Scanner s = new Scanner(System.in);
		
		String n = "";
		System.out.println("Nome do produto: ");
		n = s.nextLine();
		int cod = 0;
		System.out.println("Código do produto: ");
		cod = s.nextInt();
		float p = 0;
		System.out.println("Preço do produto: ");
		p = s.nextFloat();
		IPagamento re = null;
		
		System.out.println("Nome: " + n);
		System.out.println("Código: " + cod);
		System.out.println("Preço: " + p);
		
	}
	
}
