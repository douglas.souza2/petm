package br.edu.cest.pet.inter;

public interface IPagamento {
	public String getNome();
	public Integer getCodigo();
	public float getPreco(); 
	public String toString();
}
