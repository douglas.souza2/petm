package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public class Raposa extends Animal {

	public Raposa(String nome) {
		super(nome);
	}

	@Override
	public String getFala() {
		return "uiva";
	}

	@Override
	public String getEspecie() {
		return "raposa";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}
	

}
