package br.edu.cest.pet.entity;

public class Funcionario extends PessoaFisica {
	private int matricula;
	private int experiencia;
	
	public Funcionario(String nome,String cpf) {
		super(nome,cpf);
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	
	@Override
	public String toString() {
	     return "Mat.:" + getMatricula() 
	     + " - Nome:" + getNome();
    }

	public void setExperiencia(int exp) {
		experiencia = exp;
		
	}
	
	public int getExperiencia() {
		return experiencia;
	}
	
}
