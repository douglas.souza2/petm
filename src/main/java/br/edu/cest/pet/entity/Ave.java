package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public class Ave extends Animal {

	public Ave(String nome) {
		super(nome);
		
	}

	@Override
	public String getFala() {
		return "grasma";
	}

	@Override
	public String getEspecie() {
		return "ave";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.BIPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.GRAMA;
	}

}
