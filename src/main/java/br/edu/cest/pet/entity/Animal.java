package br.edu.cest.pet.entity;

import java.math.BigDecimal;

import br.edu.cest.pet.EnumLocomocao;

public abstract class Animal implements AnimalIFC {
         private String nome;
         private BigDecimal peso;
         private String fala;
         
         public Animal(String nome) { //construtor
        	 this.nome = nome;
         }
         
         
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public BigDecimal getPeso() {
			return peso;
		}
		public void setPeso(BigDecimal peso) {
			this.peso = peso;
		}
         
         public String toString() {
        	 StringBuffer sb = new StringBuffer();
        	 sb.append("Nome: " + getNome());
        	 sb.append("\nFala: " + getFala());
        	 sb.append("\nPatas: " + getLocomocao());
        	 return sb.toString();
        	 
         }
         
         public EnumPeso getUnidadePeso() {
        	 return EnumPeso.GRAMA;
         }
         
         public BigDecimal getPeso(EnumPeso un) {
        	 if (un.equals("g"))
        		 return getPeso().multiply(BigDecimal.valueOf(1000));
        	 else return getPeso();
         }
         
}
