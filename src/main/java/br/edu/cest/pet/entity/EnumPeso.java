package br.edu.cest.pet.entity;

public enum EnumPeso {
	QUILO("kg"),
	GRAMA("g");
	
	String unidade;
	
	private EnumPeso(String un) {
		this.unidade = un;
	}

}
