package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public class Equino extends Animal {

	public Equino(String nome) {
		super(nome);
	}

	@Override
	public String getFala() {
		return "relincha";
	}

	@Override
	public String getEspecie() {
		return "equino";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}

}
