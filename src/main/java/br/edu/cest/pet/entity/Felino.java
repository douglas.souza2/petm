package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public class Felino extends Animal {

	public Felino(String nome) {
		super(nome);
	}

	@Override
	public String getFala() {
		return "mia";
	}

	@Override
	public String getEspecie() {
		return "felino";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}

}
