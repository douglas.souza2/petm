package br.edu.cest.pet.entity;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Area<R> extends ArrayList<R> {
         private String nome;
         private BigDecimal humidade;
         private BigDecimal temperatura;
         
         public Area(String nome) {
        	 this.nome = nome;
         }
         
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public BigDecimal getHumidade() {
			return humidade;
		}
		public void setHumidade(BigDecimal humidade) {
			this.humidade = humidade;
		}
		public BigDecimal getTemperatura() {
			return temperatura;
		}
		public void setTemperatura(BigDecimal temperatura) {
			this.temperatura = temperatura;
		}
         
}
