package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public interface AnimalIFC {
	public String getFala();
	public String getEspecie();
	public EnumLocomocao getLocomocao();

}
