package br.edu.cest.pet.entity;

public class Veterinario extends Funcionario {
	private String crmv;
	
	public Veterinario(String nome,String cpf) {
		super(nome,cpf);
	}

	public String getCrmv() {
		return crmv;
	}

	public void setCrmv(String crmv) {
		this.crmv = crmv;
	}
	
}
