package br.edu.cest.pet.entity;

import java.util.Date;

public class PessoaFisica {
	private String nome;
	private String cpf;
	private Date dtaNasc;
	
	public PessoaFisica() {
		// TODO
	}
	
	public PessoaFisica(String nome, String cpf) {  // construtor
		this.nome = nome;                                   // PessoaFisica(String String)
	}
	
	public PessoaFisica(String nome) {  // PessoaFisica(String)
		this.nome = nome;
	}
	
	public PessoaFisica(String nome, Date nasc) {  // PessoaFisica(String Date)
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getDtaNasc() {
		return dtaNasc;
	}
	public void setDtaNasc(Date dtaNasc) {
		this.dtaNasc = dtaNasc;
	}

}
