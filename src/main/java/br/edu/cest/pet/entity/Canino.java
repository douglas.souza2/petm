package br.edu.cest.pet.entity;

import br.edu.cest.pet.EnumLocomocao;

public class Canino extends Animal {

	public Canino(String nome) {
		super(nome);
	}

	@Override
	public String getFala() {
		return "late";
	}

	@Override
	public String getEspecie() {
		return "canino";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.QUADRUPEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.QUILO;
	}

}
