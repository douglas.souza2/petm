package br.edu.cest.pet.rh;

import java.util.Scanner;

import br.edu.cest.pet.Empresa;
import br.edu.cest.pet.entity.Funcionario;
import br.edu.cest.pet.entity.Veterinario;

public class RH {
	Empresa e;

	public RH(Empresa emp) {
		this.e=emp;
	}

	public void contrataVeterinario() {
		Scanner s = new Scanner(System.in);
		System.out.println("Nome: ");
		String nome = s.nextLine();
		System.out.println("Experiencia(anos): ");
		int exp = s.nextInt();
		System.out.println("CPF: ");
		String cpf = s.nextLine();
		Funcionario vet = new Veterinario(nome, cpf);
		vet.setExperiencia(exp);
		s.close();
	}

	public void contrataAtendente() {
		// TODO Auto-generated method stub
		
	}
	
}
