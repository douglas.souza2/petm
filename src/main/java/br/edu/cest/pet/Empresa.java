package br.edu.cest.pet;

import java.util.ArrayList;

import br.edu.cest.pet.entity.Funcionario;

public class Empresa {
	private String nome;
	private String cnpj;
	private ArrayList<Funcionario> funcs;
	private ArrayList<Cliente> clientes;
	private ArrayList<Fornecedor> fornecedores;
	
	public Empresa(String nome , String cnpj) {
		this.nome = nome;
		this.cnpj = cnpj;
	}
	
	@Override
	public String toString() {
		return "Empresa: " + nome + " / " + cnpj;
	}

}
