package br.edu.cest.pet;

import java.awt.geom.Area;
import java.util.ArrayList;

import br.edu.cest.pet.Financeiro.Contador;
import br.edu.cest.pet.entity.Animal;
import br.edu.cest.pet.entity.Canino;
import br.edu.cest.pet.entity.Felino;
import br.edu.cest.pet.entity.Funcionario;
import br.edu.cest.pet.entity.PessoaFisica;
import br.edu.cest.pet.entity.Raposa;
import br.edu.cest.pet.entity.Veterinario;
import br.edu.cest.pet.rh.RH;

/*
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
    	Raposa r = new Raposa("Fox");
    	Canino c = new Canino("Rex");
    	
    	ArrayList<Animal> l = new ArrayList<Animal>();
    	l.add(r);
    	l.add(c);
    	
    	for (Animal a:l) {
    		System.out.println("---------- \n" + a);
    	}
    	
    	/*
    	App a = new App();
    	if (args.length == 2) {
    	    a.inicio(args[0], args[1]);
    	} else {
    		System.out.println("Nome e CNPJ");
    	}
    }
    
    private void inicio(String nome, String cnpj) {
    	Contador c = new Contador();
    	Empresa e = c.fundaEmpresa(nome, cnpj);
    	
    	RH rh = new RH(e);
    	rh.contrataVeterinario();
    	rh.contrataAtendente();
    }
    	*/
    	/*
        System.out.println( "Hello World!" );
        PessoaFisica pf = new PessoaFisica("José");
        System.out.println("Nome: " + pf.getNome());
        Funcionario func = new Funcionario("Anibal", "1234");
        System.out.println("Nome func: " + func.getNome());
        
        Veterinario vet = new Veterinario("Asdrubal", "5678");
        System.out.println("Nome vet: " + vet.getNome());
        
        Felino f = new Felino("Frajola");
        System.out.println(f.toString());
        
        Felino f2 = new Felino("Tom");
        System.out.println(f2.toString());
        
        String[] arrTexto = {
        		"Teste", // 0
        		"Elemento2", // 1
        		"Elemento3" // 2
        };
        
        String[] texto2 = new String[5];
        texto2[3] = "Mais um elemento";
        
        System.out.println(arrTexto.length);
        
        System.out.println(arrTexto[1]);
        System.out.println(texto2[4]);
        System.out.println(texto2[3]);
        
        for(int i=0;i<arrTexto.length;i++) {
        	System.out.println(arrTexto[i]);
        }
        int[] arrInt;
        Animal[] arrAnimal;
       //  Ave[] arrAve;
        
        ArrayList lista = new ArrayList();
        lista.add(f);
        lista.add(f2);
        Canino c = new Canino("Bafo de Onca");
        lista.add(c);
       //  lista.add(arrTexto);
        for (int i=0;i<lista.size();i++) {
        	System.out.println("--------");
        	System.out.println(lista.get(i));
        }
        */
    	/*
    int x = 2;
    int y = 4;
    int i = 1;
    
    y=+1;
    
    System.out.println("Saida[0]: i=" + (++i) + "x=" + (x++) + "y=" + (y++));
    i += 6;
    x += ++i;
    System.out.println("Saida[1]: i=" + (i++) + "x=" + (--x) + "y=" + (++y));
    y += (3)*(i+x%2);
    x += 2;
    i -= 3;
    System.out.println("Saida[2]: i=" + (++i) + "x=" + (x++) + "y=" + (y++));
    */
    	
        /*
        // Generics
        Area<Felino> listaF = new Area<Felino>("Area Jerry");
        listaF.add(f);
        listaF.add(f2);
        for(Felino fel:listaF) {
       	System.out.println("--------");
        	System.out.println(fel);
        }
        
        Area<Canino> listaC = new Area<Canino>("Area Tom");
        listaC.add(c);
        
        */
    }
    
    
}
